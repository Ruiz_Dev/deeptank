﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lever : MonoBehaviour {

    public int state = 0;
    private Transform spriteChild;
    private Transform baseOfLever;
    private BoxCollider2D bc2d;
    [HideInInspector] public bool leverMoved;
    //private GameObject feature; //object to activate

	// Use this for initialization
	void Start () {
        spriteChild = transform.GetChild(0);
        baseOfLever = transform.GetChild(1);
        bc2d = GetComponent<BoxCollider2D>();

    }
	
	// Update is called once per frame
	void Update () {


        
	}

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "Player")
        {
            if(col.gameObject.GetComponent<Player>().velocity.x > 0)//moving right
            {
                if (state != 1) {
                    state++;
                    spriteChild.RotateAround(baseOfLever.position, Vector3.forward, -30);
                    bc2d.size = (state == 0) ? new Vector2(0.2f, bc2d.size.y) : new Vector2(0.5f, bc2d.size.y);
                    bc2d.offset = (state == 0) ? new Vector2(0, 0) : new Vector2(0.2f, 0);
                    leverMoved = true;
                    FindObjectOfType<AudioManager>().PlayAudio("Lever");
                }
            }else //moving left
            {
                if (state != -1) {
                    state--;
                    spriteChild.RotateAround(baseOfLever.position, Vector3.forward, 30);
                    bc2d.size = (state == 0) ? new Vector2(0.2f, bc2d.size.y) : new Vector2(0.5f, bc2d.size.y);
                    bc2d.offset = (state == 0) ? new Vector2(0, 0) : new Vector2(-0.2f, 0);
                    leverMoved = true;
                    FindObjectOfType<AudioManager>().PlayAudio("Lever");
                }
            }

            col.GetComponent<Player>().animator.SetTrigger("ActivateLever");

            
        }
    }
}
