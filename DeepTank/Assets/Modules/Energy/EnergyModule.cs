﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnergyModule : ModuleScript
{
    public static int numberOfEnergyModules = 0;
    [Range(0,100)]
    [SerializeField] private float energyPercentatge;
    Wheel wheel;
    public float energyMultiplier = 10f;

    //Energy bar visibility
    Transform batteryIndicator;
    Transform[] energyBars = new Transform[5];
    Material barMat;

    // Use this for initialization
    protected override void Start()
    {
        base.Start();
        numberOfEnergyModules++;
        submarine.maxEnergy += 100f;
        submarine.totalEnergy += 100f;
        //get reference to energy bars
        batteryIndicator = transform.GetChild(5);
        for (int i = 0; i < 5; i++)
        {
            energyBars[i] = batteryIndicator.GetChild(i);
            energyBars[i].GetComponent<MeshRenderer>().material = barMat;
        }

        wheel = transform.GetChild(4).GetComponent<Wheel>();
    }

    // Update is called once per frame
    protected override void Update()
    {
        base.Update();

        //add Energy when spin wheel
        if (wheel.moveWheelValue > 0)
        {
            submarine.totalEnergy += wheel.moveWheelValue * energyMultiplier * numberOfEnergyModules;
            submarine.totalEnergy = Mathf.Clamp(submarine.totalEnergy, 0, submarine.maxEnergy);
        }
        energyPercentatge = submarine.totalEnergy / numberOfEnergyModules; //calculate energy% of this module

        //control visibility of energy bars
        for (int i = 0; i < 5; i++) energyBars[i].gameObject.SetActive(false);
        setBarsColor(Color.red);

        if(energyPercentatge > 0)
        {
            energyBars[0].gameObject.SetActive(true);
            if(energyPercentatge > 20)
            {
                energyBars[1].gameObject.SetActive(true);
                setBarsColor(Color.yellow);
                if (energyPercentatge > 40)
                {
                    energyBars[2].gameObject.SetActive(true);
                    if (energyPercentatge > 60)
                    {
                        energyBars[3].gameObject.SetActive(true);
                        setBarsColor(Color.green);
                        if (energyPercentatge > 80)
                        {
                            energyBars[4].gameObject.SetActive(true);
                        }
                    }
                }
            }
        }
        
    }

    private void setBarsColor(Color c)
    {
        for(int i = 0; i < 5; i++)
        {
            energyBars[i].GetComponent<MeshRenderer>().material.color = c;
        }
    }

    public override void ResetModule()
    {
        Debug.Log("Module reseted from child class");
    }
}
