﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HookModule : ModuleScript {

    public Button buttonHook;
    public Hook hook;
    private GameObject animalHooked;
	// Use this for initialization
	protected override void Start () {
        base.Start();
    }
	
	// Update is called once per frame
	protected override void Update () {
        base.Update();
        if (buttonHook.isDown())
        {
            hook.hookState = Hook.State.Descending;
            if(hook.GetTheBaitAnimal() != null)
            {
                hook.GetTheBaitAnimal().GetComponent<Animal>().SetAnimalState(Animal.State.Baited);
                hook.thereIsBait = true;
                SeaManager.instanceSeaManager.SharkState();
            }
            buttonHook.MakeButtonUp();
        }

        
	}

    /* public void SetAnimalHooked(GameObject _animalHooked)
     {
         animalHooked = _animalHooked;
     }*/

    public override void ResetModule()
    {
        Debug.Log("Module reseted from child class");
    }
}
