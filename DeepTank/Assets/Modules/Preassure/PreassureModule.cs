﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PreassureModule : ModuleScript {

    float waterQuantity; //% of water in the tank
    float lastWaterQ;
    Button bLess, bMore; //less water and more water
    [SerializeField] private static float buttonMultiplier = 10;
    [SerializeField] private static float energyLoss = 5f;
    float waterScaledY;
    float waterBasePosY;
    Transform waterIndicator;
    bool usingAnyButton = false;

	// Use this for initialization
	protected override void Start () {
        base.Start();
        bLess = transform.GetChild(4).GetComponent<Button>();
        bMore = transform.GetChild(5).GetComponent<Button>();
        waterIndicator = transform.GetChild(7);
        waterScaledY = waterIndicator.localScale.y;
        waterBasePosY = waterIndicator.localPosition.y-waterScaledY/2f;
        waterIndicator.localScale = new Vector3(waterIndicator.localScale.x, 0, 1);
        waterIndicator.localPosition = new Vector3(waterIndicator.localPosition.x, waterBasePosY, waterIndicator.localPosition.z);
    }
	
	// Update is called once per frame
	protected override void Update () {
        base.Update();

        //change goal water value if any button is pressed
        if (bLess.isDown() && submarine.totalEnergy >0)
        {
            waterQuantity -= buttonMultiplier*Time.deltaTime;
            waterQuantity = Mathf.Clamp(waterQuantity, 0f, 100f);
            usingAnyButton = true;
            
            //energy
            submarine.totalEnergy -= energyLoss * Time.deltaTime;
            submarine.totalEnergy = Mathf.Max(submarine.totalEnergy, 0);

        }

        if (bMore.isDown() && submarine.totalEnergy > 0)
        {
            waterQuantity += buttonMultiplier*Time.deltaTime;
            usingAnyButton = true;
            waterQuantity = Mathf.Clamp(waterQuantity, 0f, 100f);

            //energy
            submarine.totalEnergy -= energyLoss * Time.deltaTime;
            submarine.totalEnergy = Mathf.Max(submarine.totalEnergy, 0);
        }

        if (usingAnyButton)
        {
            waterIndicator.localScale = new Vector3(waterIndicator.localScale.x, waterQuantity * waterScaledY / 100f, 1);
            waterIndicator.localPosition = new Vector3(waterIndicator.localPosition.x, waterBasePosY + waterIndicator.localScale.y / 2f, waterIndicator.localPosition.z);
            usingAnyButton = false;

            //change submarine value
            submarine.depthGoal += (waterQuantity - lastWaterQ) * submarine.maxDepthPerModule / 100f;
            lastWaterQ = waterQuantity;

            
        }

        
    }
}
