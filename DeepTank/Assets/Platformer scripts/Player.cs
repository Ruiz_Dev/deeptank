﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent (typeof (Controller2D))]
public class Player : MonoBehaviour {

	public float maxJumpHeight = 4;
	public float minJumpHeight = 1;
	public float timeToJumpApex = .4f;
	float accelerationTimeAirborne = .2f;
	float accelerationTimeGrounded = .1f;
	float moveSpeed = 7;

	float gravity;
	float maxJumpVelocity;
	float minJumpVelocity;
    [HideInInspector]
	public Vector3 velocity;
    float targetVelocityX;
    float velocityXSmoothing;
    public bool canMove = true;

	Controller2D controller;
	public SpriteRenderer spriteHead, spriteBody, spriteLeftArm, spriteRightarm, spriteLeftFoot, spriteRigthFoot;

	Vector2 directionalInput;
	bool isMoving = false;
	bool isFacingRight;

    [Range(1,2)]
    public int playerID;
    [HideInInspector] public KeyCode upKey;
    [HideInInspector] public string horizontalAxis, verticalAxis;
    [HideInInspector] public KeyCode handleObject;

    private Hook hook;
    [SerializeField]private GameObject animalGrab = null;
    private float cooldownDropObject = 0.5f;
    private bool canDropObject;
    [HideInInspector]public bool canGrabAnimalHooked = false;


    [HideInInspector]public Animator animator;
    void Start() {
		controller = GetComponent<Controller2D> ();
		//sprite = GetComponent<SpriteRenderer> ();
        animator = GetComponent<Animator>();

		gravity = -(2 * maxJumpHeight) / Mathf.Pow (timeToJumpApex, 2);
		maxJumpVelocity = Mathf.Abs(gravity) * timeToJumpApex;
		minJumpVelocity = Mathf.Sqrt (2 * Mathf.Abs(gravity) * minJumpHeight);
		//print ("Gravity: " + gravity + "  Jump Velocity: " + maxJumpVelocity);

        if(playerID == 1)
        {
            upKey = KeyCode.UpArrow;
            horizontalAxis = "Horizontal";
            verticalAxis = "Vertical";
            handleObject
                = KeyCode.RightShift;
        }
        else if (playerID == 2)
        {
            upKey = KeyCode.W;
            horizontalAxis = "Horizontal2";
            verticalAxis = "Vertical2";
            handleObject = KeyCode.LeftShift;
        }

    }

    void Update() {

        if (canMove)
        {
            directionalInput = new Vector2(Input.GetAxisRaw(horizontalAxis), Input.GetAxisRaw(verticalAxis));
            velocity.x = Mathf.SmoothDamp(velocity.x, targetVelocityX, ref velocityXSmoothing, (controller.collisions.below) ? accelerationTimeGrounded : accelerationTimeAirborne);
            
            velocity.y += gravity * Time.deltaTime;
        }
        else //cant move
        {
            directionalInput = new Vector2(0,0);
            velocity.x = 0;
            velocity.y = 0;
        }
        targetVelocityX = directionalInput.x * moveSpeed;


		//animation control
        
		if (!isMoving && Mathf.Abs(targetVelocityX) > 0) {
			isMoving = true;
		} else if(isMoving && Mathf.Abs(velocity.x) < 5){
			isMoving = false;
		}

        animator.SetFloat("Velocity.x", Mathf.Abs(Input.GetAxisRaw(horizontalAxis)));
                
        controller.Move(velocity * Time.deltaTime, directionalInput);

        if (controller.collisions.above || controller.collisions.below)
        {
            if (controller.collisions.slidingDownMaxSlope)
            {
                velocity.y += controller.collisions.slopeNormal.y * -gravity * Time.deltaTime;
            }
            else
            {
                velocity.y = 0;
                animator.SetBool("isJumping", false);
            }
        }


        //JUMP
        if (canMove)
        {
            if (Input.GetKeyDown(upKey))
            {
                FindObjectOfType<AudioManager>().PlayAudio("Jump");
                animator.SetBool("isJumping", true);
                if (controller.collisions.below)
                {
                    if (controller.collisions.slidingDownMaxSlope)
                    {
                        if (directionalInput.x != -Mathf.Sign(controller.collisions.slopeNormal.x))
                        {//not jumping against max slope
                            velocity.y = maxJumpVelocity * controller.collisions.slopeNormal.y;
                            velocity.x = maxJumpVelocity * controller.collisions.slopeNormal.x;
                        }
                    }
                    else
                    {
                        velocity.y = maxJumpVelocity;
                    }
                }
            }

            if (Input.GetKeyUp(upKey))
            {
                if (velocity.y > minJumpVelocity)
                {
                    velocity.y = minJumpVelocity;
                }
            }
        }

        
        if (Input.GetKeyDown(handleObject))
        {
            //agafar animal del gancho
            if (canGrabAnimalHooked == true && animalGrab == null)
            {
                TakeHookedAnimal();
                canGrabAnimalHooked = false;
                StartCoroutine(DropObjectClock());
                animator.SetBool("haveObject", true);
            }
            //deixar 
            else if(animalGrab != null && canDropObject)
            {
                animalGrab.transform.parent = this.gameObject.transform.parent;//perque es mantingui dintre del submari si esta en movimetn
                animalGrab = null;
                canDropObject = false;
                animator.SetBool("haveObject", true);
            }
        }
	}

    public void setEndLevel()
    {
        /*
        canMove = false;
        sprite.enabled = false;

        */


    }

    private void TakeHookedAnimal()
    {
        if(hook.hookState == Hook.State.Repose && animalGrab == null)
        {
            animalGrab = hook.GetTheBaitAnimal();
            animalGrab.transform.parent = this.gameObject.transform;
            animalGrab.GetComponent<Animal>().SetAnimalState(Animal.State.Submarine);
            animalGrab.GetComponent<Animal>().SetHookOfTheAnimal(null);
            hook.RefreshBaitHook();
            hook = null;
        }
    }

    public void SetHookPlayer(Hook _h)
    {
        hook = _h;
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Animal" && collision.gameObject.GetComponent<Animal>().GetAnimalState()== Animal.State.Submarine)
        {
            if(Input.GetKeyDown(handleObject) && animalGrab == null)
            {
                animalGrab = collision.gameObject;
                animalGrab.transform.parent = this.gameObject.transform;
                StartCoroutine(DropObjectClock());
                
            }
        }
    }

    private IEnumerator DropObjectClock()
    {
        yield return new WaitForSeconds(cooldownDropObject);
        canDropObject = true;
    }

    public bool HasAnimalInHand()
    {
        if(animalGrab == null)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public int GetAnimalPoints()
    {
        int points = 0;
        if(animalGrab.tag == "Animal")
        {
            points = animalGrab.GetComponent<Animal>().GetAnimalScore();
        }

        return points;
    }


    public void SetAnimalInAquarium(Transform t)
    {
        animalGrab.GetComponent<Animal>().SetAnimalState(Animal.State.Aquarium);
        animalGrab.transform.parent = t.gameObject.transform;
        animalGrab = null;
        canDropObject = false;
    }
}