﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Animal: MonoBehaviour {

    public enum TypeAnimal { aFish, aOctopus, aShark };
    [Header("Type of aquatic animal")]
    [SerializeField] protected TypeAnimal typeAnimal;
    [SerializeField] protected int animalScore;

    public enum State { Water, Baited, Hooked, Submarine, Aquarium };
    [Header("State")]
    [Tooltip("3 states that change the methods of the animal: water, pinza, aquarium")]
    [SerializeField] protected State state;

    [Space(10)]
    [Header("CoolDowns IA")]
    [Tooltip("Time in which have the same move direction")]
    public float moveCD;
    //[Tooltip("CurrentTime to change the behaviour")]
    protected float tempCD;

    [Space(10)]
    [Header("Move Stats")]
    [Tooltip("Current Velocity")]
    protected Vector2 moveSpeed = new Vector2(0, 0);
    protected Vector2 newPos;
    [Tooltip("Value applicat when change direcion")]
    public Vector2 moveAcceleration;
    protected Vector2 currentAccelration;
    [Tooltip("Movement max Velocity can get in two axis")]
    public Vector2 maxSpeed;

    protected Rigidbody2D myRB;
    protected Collider2D myCol;

    private Hook hookGrabed;
    protected virtual void Start()
    {
        gameObject.tag = "Animal";
    }

    public TypeAnimal GetAnimalType()
    {
        return typeAnimal;
    }

    public State GetAnimalState()
    {
        return state;
    }

    public void SetAnimalState(State _state)
    {
        state = _state;
    }

    public void SetHookOfTheAnimal(Hook _h)
    {
        hookGrabed = _h;
    }

    public Hook GetHookOfTheAnimal()
    {
        return hookGrabed;
    }

    public int GetAnimalScore()
    {
        return animalScore;
    }
    protected virtual void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Player" && state == State.Hooked)
        {
            collision.gameObject.GetComponent<Player>().canGrabAnimalHooked = true;
            collision.gameObject.GetComponent<Player>().SetHookPlayer(hookGrabed);

        }
    }
    protected virtual void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player" && state == State.Hooked)
        {
            collision.gameObject.GetComponent<Player>().canGrabAnimalHooked = false;
            collision.gameObject.GetComponent<Player>().SetHookPlayer(null);
        }
    }
}
