﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fish : Animal {

    protected override void Start ()
    {
        base.Start();
        animalScore = 3;
        typeAnimal = TypeAnimal.aFish;
        state = State.Water;

        myRB = GetComponent<Rigidbody2D>();
        myCol = GetComponent<Collider2D>();

        newPos = myRB.position;
        tempCD = moveCD;

        currentAccelration = moveAcceleration;
    }

    // Update is called once per frame
    void Update()
    {
        switch (state)
        {
            case State.Water:
                {
                    tempCD -= Time.deltaTime;
                    Move();

                    break;
                }

            case State.Hooked:
                {
                    break;
                }

            case State.Aquarium:
                {
                    break;

                }

        }
    }

    private void FixedUpdate()
    {
        switch (state)
        {
            case State.Water:
                {
                    newPos += (moveSpeed * Time.deltaTime);
                    myRB.MovePosition(newPos);
                    break;
                }

            case State.Baited:
                {

                    myRB.position = new Vector2(gameObject.transform.parent.position.x, gameObject.transform.parent.position.y - 0.5f);//a little offset
                    break;
                }

            case State.Aquarium:
                {
                    break;
                }
        }
    }

    private void Move()
    {
        SeaBounds();

        if (tempCD > 0)
        {
            moveSpeed += (currentAccelration * Time.deltaTime);

            if (moveSpeed.x >= maxSpeed.x)
            {
                moveSpeed.x = maxSpeed.x;
            }
            else if(moveSpeed.x <= maxSpeed.x * -1)
            {
                moveSpeed.x = maxSpeed.x * -1;
            }
            else if (moveSpeed.y >= maxSpeed.y)
            {
                moveSpeed.y = maxSpeed.y;
            }
            else if (moveSpeed.y <= maxSpeed.y * -1)
            {
                moveSpeed.y = maxSpeed.y * -1;
            }

        }
        else
        {
            tempCD = moveCD;
            moveSpeed = new Vector2(0, 0);
            currentAccelration.x = Random.Range(-moveAcceleration.x, moveAcceleration.x);
            currentAccelration.y = Random.Range(-moveAcceleration.y, moveAcceleration.y);
        }
    }

    private void SeaBounds()
    {
        if (transform.position.y >= 0.2)
        {
            moveSpeed.y *= -1;
        }
    }

    //protected override void OnTriggerEnter2D(Collider2D collision)
    //{
    //    base.OnTriggerEnter2D(collision);   
    //}
}
