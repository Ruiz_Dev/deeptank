﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class submarineScript : MonoBehaviour {

    [HideInInspector] public List<Transform> modules;
    [HideInInspector] public Transform modulesParent;
    [HideInInspector] public Vector2 centerAverage;
    public CameraController cam;
    public Transform centerChild;

    //first frame
    bool firstFrame = true;

    //movement variables
    [SerializeField] float speedMultiplierX = 1.5f;
    public int xMovementInputs; //the sum of all "Motor" modules inputs (lever)
    Vector2 velocity;
    float velocityXSmoothing = 0.0f; //only needed for the SmoothDamp function
    float smoothTimeX = 2.1f; //it takes this seconds to reach the desired velocity

    //preassure variables
    public float depthGoal; //the depth value to go to
    public float maxDepthPerModule = 50f;
    float velocityYSmoothing = 0.0f; //only needed for the SmoothDamp function
    [SerializeField] private float maxSpeedY = 3f;
    private float smoothTimeY = 2f;

    //energy variables
    [HideInInspector] public float maxEnergy = 0;
    [HideInInspector] public float totalEnergy;



    void Start () {
        //fill the modules array at first
        modulesParent = transform.Find("Modules");
        //Debug.Log("How many modules?: " + modules.Length);
        for (int i = 0; i < modulesParent.childCount; i++)
        {
            modules.Add(modulesParent.GetChild(i));
        }

        centerChild = transform.Find("Center");
        depthGoal = -transform.position.y; //it should be the center of the submarine, not its transform

	}
	
	// Update is called once per frame
	void Update () {
        if (firstFrame)
        {
            cam.recalculateCamera();
            firstFrame = false;
            for (int i = 0; i < modules.Count; ++i)
            {
                modules[i].GetComponent<ModuleScript>().UpdateWallTag();
            }
        }

        //energy features below
        if(totalEnergy > 0) {

            //movement of the submarine
            float targetVelocityX = xMovementInputs * speedMultiplierX;
            velocity.x = Mathf.SmoothDamp(velocity.x, targetVelocityX, ref velocityXSmoothing, smoothTimeX);
            transform.Translate(new Vector3(velocity.x * Time.deltaTime, 0f, 0f));



        }
        else
        {
            //movement of the submarine
            float targetVelocityX = 0;
            velocity.x = Mathf.SmoothDamp(velocity.x, targetVelocityX, ref velocityXSmoothing, smoothTimeX);
            transform.Translate(new Vector3(velocity.x * Time.deltaTime, 0f, 0f));
          //  FindObjectOfType<AudioManager>().StopAudio("SubmarineMovement");
            
        }

        //preassure (vertical movement) of the submarine
        velocity.y = Mathf.SmoothDamp(transform.position.y, -depthGoal, ref velocityYSmoothing, smoothTimeY, maxSpeedY);
        transform.position = new Vector3(transform.position.x, velocity.y, transform.position.z);
        FindObjectOfType<AudioManager>().PlayAudio("ChangePression");


    }

    /// <summary>
    /// Calculates the new center of the submarine considerating all the modules.
    /// It also calculates the radius of the submarine in both X and Y axis, useful for the camera.
    /// This function should be called whenever a new module is added to the submarine.
    /// </summary>
    /// <returns>The 2 radius of the submarine in the X and Y axis</returns>
    public Vector2 getSubmarineSize()
    {
        float maxX, minX, maxY, minY;
        //starting point. There is always a first module
        maxX = minX = modules[0].position.x;
        maxY = minY = modules[0].position.y;

        for(int i = 1; i < modules.Count; ++i)
        {
            Vector2 auxPos = modules[i].position;
            maxX = Mathf.Max(maxX, auxPos.x);
            minX = Mathf.Min(minX, auxPos.x);
            maxY = Mathf.Max(maxY, auxPos.y);
            minY = Mathf.Min(minY, auxPos.y);
        }

        centerAverage = new Vector2((maxX + minX) / 2f, (maxY + minY) / 2f);
        centerChild.position = new Vector3(centerAverage.x, centerAverage.y, 0f);


        return new Vector2(Mathf.Abs(centerAverage.x-maxX), Mathf.Abs(centerAverage.y-maxY));
    }
    
    /// <summary>
    /// Set all modules to default state, so it doesnt move or lose energy
    /// </summary>
    public void ResetAllModules()
    {
        for(int i = 0; i < modules.Count; ++i)
        {
            modules[i].GetComponent<ModuleScript>().ResetModule();
        }
    }

    public void AddModule(Transform newModule)
    {
        ModuleScript ms = newModule.GetComponent<ModuleScript>();
        ms.ResetAdjacents();
        for (int i = 0; i < modules.Count; ++i)
        {
            modules[i].GetComponent<ModuleScript>().UpdateAdjacent(newModule);
        }
        ms.UpdateWallTag();
        modules.Add(newModule);

        cam.recalculateCamera();

    }




}
